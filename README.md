# kicad-exports

A collection of scripts for easier kicad use in Fablab environments.

## Export as plugin

This kicad plugin creates svg exports of the F.Cu + Edge.Cuts, B.Cu + Edge.Cuts and Edge.Cuts (see [example](example)) for later use in fabmodules.

More information about this functionality in kicad [here](https://github.com/KiCad/kicad-source-mirror/blob/master/Documentation/development/pcbnew-plugins.md)

1. Open pcbnew in kicad
2. Open the scripting console

![](assets/scriptingconsole.png)

3. Type the following:

```
import pcbnew; print pcbnew.PLUGIN_DIRECTORIES_SEARCH
```

4. Find the `scripting/plugings` directory and there put `fabexports.py` and `svg_postprocessor.py`

5. Then reload plugins and enjoy!

![](assets/plugins.png)

## Tips and tricks

Below some tips to make the workflow a bit more streamline:

1. Make the outline (Edge.Cuts) of 0.8mm thick, so that the tool goes through easily in mods.

## Inkscape in command line

1. Symlink inkscape application to `/usr/local/bin/inkscape` (OSX):

```
> ln -s /Applications/Inkscape.app/Contents/MacOS/inkscape /usr/local/bin/inkscape
```

2. Make an alias for inkscape defaults:

```
> nano ~/.zshrc
```

And append to the file:

```
alias inkexp="/usr/local/bin/inkscape --export-area-drawing --export-type png --export-dpi 999 -y 255"
```

Then, in the exports folder in the kicad project:

```
> cd exports
> ls
bar-1ch-levelshifter-Edge_Cuts.svg
bar-1ch-levelshifter-B_Cu.svg
bar-1ch-levelshifter-F_Cu.svg

> inkexp *
(process:89957): Gtk-WARNING **: 20:45:24.419: Locale not supported by C library.
        Using the fallback 'C' locale.
Background RRGGBBAA: ffffff00
Area 368.502:162.889:435.776:215.053 exported to 999 x 775 pixels (1425.57 dpi)
Background RRGGBBAA: ffffff00
Area 368.502:162.889:435.776:215.053 exported to 999 x 775 pixels (1425.57 dpi)
Background RRGGBBAA: ffffff00
Area 368.502:162.889:435.776:215.053 exported to 999 x 775 pixels (1425.57 dpi)
> ls
bar-1ch-levelshifter-B_Cu.png      bar-1ch-levelshifter-Edge_Cuts.svg
bar-1ch-levelshifter-B_Cu.svg      bar-1ch-levelshifter-F_Cu.png
bar-1ch-levelshifter-Edge_Cuts.png bar-1ch-levelshifter-F_Cu.svg
```

And voilà, all the .svgs were converted to .pngs, ready to go to fabmodules.

## Refs

Heavily borrowed from https://github.com/scottbez1/splitflap/blob/580a11538d801041cedf59a3c5d1c91b5f56825d/electronics/generate_svg.py
From https://www.andresworld.co.uk/how-to-invert-hex-colour-codes/