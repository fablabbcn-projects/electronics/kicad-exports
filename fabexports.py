from pcbnew import *
import os
import time
from svg_processor import SvgProcessor
import shutil

'''
From https://www.andresworld.co.uk/how-to-invert-hex-colour-codes/
Adapted for Fablab stuff by oscgonfer
'''

def invertHex(hexNumber):
    #invert a hex number
    inverse = hex(abs(int(hexNumber, 16) - 255))[2:] 
    # if the number is a single digit add a preceding zero
    if len(inverse) == 1: 
        inverse = '0' + inverse
    return inverse

class FabExports(ActionPlugin):
    """
    test_by_date: A sample plugin as an example of ActionPlugin
    Add the date to any text field of the board where the content is '$date$'
    How to use:
    - Add a text on your board with the content '$date$'
    - Call the plugin
    - Automaticaly the date will be added to the text (format YYYY-MM-DD)
    """

    def defaults(self):
        """
        Method defaults must be redefined
        self.name should be the menu label to use
        self.category should be the category (not yet used)
        self.description should be a comprehensive description
          of the plugin
        """
        self.name = "Fablab Exports"
        self.category = "Export"
        self.description = "Export kicad PCB to SVG and PNG"

    def Run(self):

        def colorInvert(original):
            #define an empty string for our new colour code
            inverse = "" 
            # if the code is RGB
            R = original[1:3]
            G = original[3:5]
            B = original[5:]
            inverse = inverse + invertHex(R)
            inverse = inverse + invertHex(G)
            inverse = inverse + invertHex(B)
            return '#' + inverse
        
        board = GetBoard()
        pc = PLOT_CONTROLLER(board)
        po = pc.GetPlotOptions()

        # Set options
        po.SetAutoScale(False)
        po.SetScale(1)
        po.SetMirror(False)
        po.SetOutputDirectory('exports')
        po.SetExcludeEdgeLayer(False)
        po.SetPlotFrameRef(False)
        po.SetDrillMarksType(PCB_PLOT_PARAMS.FULL_DRILL_SHAPE)

        for layer in ["F.Cu", "B.Cu", "Edge.Cuts", "Drills"]:
                        
            # Set current layer
            if layer == "F.Cu":
                pc.OpenPlotfile(layer, PLOT_FORMAT_SVG, layer)
                pc.SetLayer(F_Cu)
                pc.PlotLayer()

                fcu_name = pc.GetPlotFileName()

            if layer == "B.Cu":
                pc.OpenPlotfile(layer, PLOT_FORMAT_SVG, layer)
                pc.SetLayer(B_Cu)
                pc.PlotLayer()

            if layer == "Edge.Cuts": 
                pc.OpenPlotfile(layer, PLOT_FORMAT_SVG, layer)
                pc.SetLayer(Edge_Cuts)
                pc.PlotLayer()

            if layer == "Drills":
                processor = SvgProcessor(fcu_name)
                processor.apply_color_transform(colorInvert)
                filename = fcu_name.strip('F_Cu.svg') + 'Drills.svg'
                processor.write(filename)

            pc.ClosePlot()

FabExports().register()